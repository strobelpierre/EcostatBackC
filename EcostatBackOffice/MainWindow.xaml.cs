﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;

namespace EcostatBackOffice
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

            InitializeComponent();
            FillDataGridView();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ListAllTheme(object sender, MouseButtonEventArgs e)
        {
           
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ThemePanel.IsSelected)
            {
               
                

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            FillDataGridView();
            // Chargez les données dans la table sondage. Vous pouvez modifier ce code selon les besoins.
            
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            FillDataGridView();
        }


        protected void FillDataGridView()
        {
            var context = new ecostatEntities1();
            context.theme.Load();
            EcostatBackOffice.ecostatDataSet ecostatDataSet = ((EcostatBackOffice.ecostatDataSet)(this.FindResource("ecostatDataSet")));
            // Chargez les données dans la table theme. Vous pouvez modifier ce code selon les besoins.
            EcostatBackOffice.ecostatDataSetTableAdapters.themeTableAdapter ecostatDataSetthemeTableAdapter = new EcostatBackOffice.ecostatDataSetTableAdapters.themeTableAdapter();
            ecostatDataSetthemeTableAdapter.Fill(ecostatDataSet.theme);
            System.Windows.Data.CollectionViewSource themeViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("themeViewSource")));
            themeViewSource.Source = context.theme.Local;
            themeViewSource.View.MoveCurrentToFirst();
            EcostatBackOffice.ecostatDataSetTableAdapters.sondageTableAdapter ecostatDataSetsondageTableAdapter = new EcostatBackOffice.ecostatDataSetTableAdapters.sondageTableAdapter();
            ecostatDataSetsondageTableAdapter.Fill(ecostatDataSet.sondage);
            System.Windows.Data.CollectionViewSource sondageViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("sondageViewSource")));
            sondageViewSource.View.MoveCurrentToFirst();
        }

        private void addtheme(object sender, RoutedEventArgs e)
        {
    
            var windows = new editTheme();
            windows.Show();
            
        }

        private void DelThemeAction(object sender, RoutedEventArgs e)
        {
           
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Etes  vous sur?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                var bdd = new ecostatEntities1();

                dynamic theme = themeDataGrid.SelectedItem;
                try
                {
                    int ID = theme.id;
                    var deletedService = bdd.theme.Find(ID);
                    if (deletedService != null)
                    {
                        bdd.theme.Remove(deletedService);
                        bdd.SaveChanges();
                        themeDataGrid.Items.Refresh();
                        themeDataGrid.UpdateLayout();

                        MessageBox.Show("Suppressions reussiite", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                        FillDataGridView();
                    }
                }
                catch (Exception s )
                {
                    MessageBox.Show(s.ToString(), "bbd error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            

            }
        }

        private void EditTheme_Click(object sender, RoutedEventArgs e)
        {
            dynamic theme = themeDataGrid.SelectedItem;
            var df = new editform(theme);
            df.Show();
           
        }

        private void AddSondageView(object sender, RoutedEventArgs e)
        {
            var s = new AddSondage();
            s.Show();

        }
    }
}
