﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EcostatBackOffice
{
    /// <summary>
    /// Logique d'interaction pour addTheme.xaml
    /// </summary>
    public partial class editTheme : Window
    {
        public editTheme()
        {
            InitializeComponent();
        }

        private void addThemeValidate_Click(object sender, RoutedEventArgs e)
        {
            var bdd = new ecostatEntities1();
            var n = new theme();
            n.Nom = nom.Text;
            bdd.theme.Add(n);
            bdd.SaveChanges();
           
           
        }
    }
}
