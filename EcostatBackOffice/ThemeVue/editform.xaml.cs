﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EcostatBackOffice
{
    /// <summary>
    /// Logique d'interaction pour addTheme.xaml
    /// </summary>
    public partial class editform: Window
    {
        private theme toedit { get; set; }
        public editform(theme edit)
        {
            InitializeComponent();
            toedit = edit;
            nom.Text = edit.Nom;


        }



        private void editThemeValidate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var db = new ecostatEntities1();
                var result = db.theme.Find(toedit.id);
                if (result != null)
                {
                    
                    db.theme.Attach(result);
                    result.Nom = toedit.Nom;    
                    db.SaveChanges();
                }
            }
            catch (Exception s)
            {
                MessageBox.Show(s.ToString(), "bbd error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
