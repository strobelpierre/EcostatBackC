//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EcostatBackOffice
{
    using System;
    using System.Collections.Generic;
    
    public partial class quizz_reponses
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public quizz_reponses()
        {
            this.utilisateur = new HashSet<utilisateur>();
        }
    
        public int id { get; set; }
        public Nullable<int> question_id { get; set; }
        public string contenu { get; set; }
    
        public virtual quizz_questions quizz_questions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<utilisateur> utilisateur { get; set; }
    }
}
